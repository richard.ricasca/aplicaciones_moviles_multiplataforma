import 'package:flutter/material.dart';

class ContadorPage extends StatefulWidget {
  @override
  createState() {
    return _ContadorPageState();
  }
}

  class _ContadorPageState extends State<ContadorPage> {
      int _conteo = 0;

      @override 
      Widget build(BuildContext context){
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Stateful',
          home: Scaffold(
            appBar: AppBar(
              title: Text('LABORATORIO 11'),
            ),
            body: Center(
              child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('click numero',style: TextStyle(fontSize: 25)),
                Text('$_conteo', style:TextStyle(fontSize: 25))
              ],
            ),
          ),

          floatingActionButton: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
                FloatingActionButton(
                  child: Icon(Icons.remove),
                onPressed: ()=>{
                  setState((){
                    _conteo > 0 ? _conteo-- : _conteo=0;
                  })
                },
                ),
                 FloatingActionButton(
                  child: Icon(Icons.exposure_zero),
                onPressed: ()=>{
                  setState((){
                    _conteo=0;
                  })
                },
                ),
                 FloatingActionButton(
                  child: Icon(Icons.add),
                onPressed: ()=>{
                  setState((){
                    _conteo++;
                  })
                },
                ),
                
            ],
          ),
          )
        );
      }}
  