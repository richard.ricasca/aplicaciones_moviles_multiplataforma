import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  final conteo =10;

  @override 
 
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home:Scaffold(
        appBar:AppBar(
          title: Text('Material App Bar'),
        ),
        body: Center(child: Column(mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('numero de clicks', style: TextStyle(fontSize: 25.0)),
          Text('$conteo', style: TextStyle(fontSize: 25.0))
        ],),),
        
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: ()=>{
            print("Hola Mundo")
          },
          ),
          ),
    );
  }
 }
 