import React from 'react';
import {View,Text,TextInput,StyleSheet} from 'react-native';

const AgeValidator = props =>(

    <View>
        <Text style={styles.texto}>Ingrese su Edad:</Text>
        <TextInput 
            style={{height:40, borderColor:'gray',borderWidth:1}}
            onChangeText={props.onTextChange}
            value={props.value}
        />

    </View>
)
const styles = StyleSheet.create({
    
    texto: {
      fontSize: 24,
    },
   
  });



export default AgeValidator;

