
import React,{Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import AgeValidator from './components/AgeValidator';

import MyList from './components/mi_listado';



export default class App extends Component{
  
  constructor(props){
    super(props);
    this.state={
      inputText:'',
      infoText:'Ingrese edad',
    }

  }
  
  
  onInputTextChange = (text) =>{
    
    if(!isNaN(text)){
      this.setState({inputtext: text});
      text>=18 ? this.setState({infoText: 'Eres mayor de edad'}): this.setState({infoText:"Eres menor de edad"})
    }
    else this.setState({inputtext: ''});
    

  }
  


  
  render(){
    return(
    <View>
      <Text style={styles.texto}>LABORATORIO 03 </Text>
      <AgeValidator 
           onTextChange={this.onInputTextChange}
           value={this.state.inputtext}
           />
           <Text style={styles.texto}>
            {this.state.infoText}
          </Text>
          <Text style={styles.texto}>Mi Listado Street Fighter:</Text>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>

          

          
           
          

         <MyList></MyList>

        </ScrollView>
      </SafeAreaView>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  texto: {
    fontSize: 34,
  },
  edad: {
    fontSize: 24,
  },
 
});


