import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Alert,
  Button,
} from 'react-native';

import OurFlatList from './components/OurFlatList';
import ConexionFetch from './components/ConexionFetch';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Mensaje from './components/Message';
import Login from './components/login';
const Stack = createStackNavigator();


function HomeScreen({navigation}){
  return(
    <View style={styles.container}>
      
      <Login/>
      
      
      <Button style={styles.boton}
        title="Ir a Listado"
        onPress={()=>navigation.navigate('Listado')}
      />
      <Button style={styles.boton}
        title="Ir a Detalles"
        onPress={()=>navigation.navigate('Detalles')}
      />
    </View>
  )
}

function ListadoScreen({navigation}){
  return(
    <View style={styles.container}>
      <ConexionFetch/>
      <Text style={styles.texto}>Estamos en Listado</Text> 
           
      <Button style={styles.boton}
        title="Ir a Detalles"
        onPress={()=>navigation.navigate('Detalles')}
      />
      <Button style={styles.boton}
        title = "Ir a Inicio"
        onPress ={()=> navigation.popToTop()}
      />
      
    </View>
  )
}

function DetalleScreen({navigation}){
  return(
    <View style={styles.container}>
      <Text style={styles.texto}>Estamos en Detalles</Text>

      <Button style={styles.boton}
        title = "Ir a Inicio"
        onPress ={()=> navigation.popToTop()}
      />
    </View>
  )
}


class App extends Component {
  onItemClick(e) {
    console.warn('You selected the movie: ' + e);
  }

  showAlert = () => {
    Alert.alert(
      'Titulo',
      'Mensaje',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => console.log('OK pressed')},
      ],
      {cancelable: false},
    );
  };

  render() {
    return (
      

      <NavigationContainer>
        <Text style={styles.texto}>LABORATORIO 05 DAM</Text>
        <Stack.Navigator>
          <Stack.Screen name="Login" component={HomeScreen} />
          <Stack.Screen name="Listado" component={ListadoScreen} />
          <Stack.Screen name="Detalles" component={DetalleScreen} />
        </Stack.Navigator>
      </NavigationContainer>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center',
    justifyContent:'center'
    
  },
  texto: {
    fontSize: 24,
  },
  boton: {
    fontSize: 44,
  },
});

export default App;




















//<View style={styles.container}>
        // <ConexionFetch onItemClick={this.onItemClick} /> 
        // <OurFlatList showAlert={this.showAlert} /> 
      // </View> 
