import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {
  final options = ['Uno', 'Dos', 'Tres','Cuatro','Cinco'];  

  @override 
  Widget build (BuildContext context) {
    return Scaffold(appBar: AppBar(
      title: Text('Componentes'),
    ),
    body:Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children:[
         Image.asset('assets/images/sf_01.png'),
         Icon(
           Icons.beach_access,
           color: Colors.blue,
           size: 36.0,
         ),
        ],
      ),
    ),
    );
    
  }
}

  /*

  List<Widget> _crearItemsCorta1(){
    return options.map((item){
      return Column(children: <Widget>[
        ListTile(title:Text(item),subtitle: Text('cualquier cosa')),
        Divider()        
      ],
      );
    }).toList();
  }
}


*/











/*
class HomePageTemp extends StatelessWidget {
  final options = ['Uno', 'Dos', 'Tres','Cuatro','Cinco'];  

  @override 
  Widget build (BuildContext context) {
    return Scaffold(appBar: AppBar(
      title: Text('Componentes'),
    ),
    body:ListView(children: <Widget>[
      ListTile(title: Text('Uno')),
      ListTile(title: Text('Dos')),
      ListTile(title: Text('Tres')),
    ],),
    );
    
  }
}
*/


/*
  @override 
  Widget build (BuildContext context) {
    return Scaffold(appBar: AppBar(
      title: Text('Componentes'),
    ),
    body:Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children:[
          FlutterLogo(
            size: 70.0,
          ),
          FlutterLogo(
            size: 70.0,
          ),
          FlutterLogo(
            size: 70.0,
          )
        ],
      ),
    ),
    );
    
  }
}

*/

/*
 TextStyle styleText = TextStyle(color: Colors.red, fontSize: 25.0);

  @override 
  Widget build (BuildContext context) {
    return Scaffold(appBar: AppBar(
      title: Text('Componentes'),
    ),
    body:Align(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Uno', style: styleText),
          Text('Dos', style: styleText),
          Text('Tres', style: styleText),
        ],
      ),
    ),
    );
    
  }
}


*/

/*
class HomePageTemp extends StatelessWidget {
  final options = ['Uno', 'Dos', 'Tres','Cuatro','Cinco'];  

  @override 
  Widget build (BuildContext context) {
    return Scaffold(appBar: AppBar(
      title: Text('Componentes'),
    ),
    body:Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children:[
         Image.asset('assets/images/sf_01.png'),
         Icon(
           Icons.beach_access,
           color: Colors.blue,
           size: 36.0,
         ),
        ],
      ),
    ),
    );
    
  }
}

*/