import 'package:flutter/material.dart';
import 'package:semana12/src/pages/ListaHome.dart';
import 'package:semana12/src/pages/home_temp.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      debugShowCheckedModeBanner: false,
      home: ListaHome(),
    );
  }
}

