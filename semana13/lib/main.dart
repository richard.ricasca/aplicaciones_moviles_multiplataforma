import 'package:flutter/material.dart';
import 'package:semana13/tabs/ajustes.dart';
import 'package:semana13/tabs/inicio.dart';
import 'package:semana13/tabs/lugares.dart';
import 'package:semana13/tabs/registro.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final TabControl = new DefaultTabController(
     length: 4,
     child: new Scaffold(
       appBar: AppBar(
         title: new Text('Navegacion Slider'),
         bottom: new TabBar(indicatorColor:Colors.red, tabs: <Widget>[
           new Tab(icon: new Icon(Icons.home),text:'Inicio'),
           new Tab(icon: new Icon(Icons.contacts),text:'Registro'),
           new Tab(icon: new Icon(Icons.place),text:'Lugares'),
           new Tab(icon: new Icon(Icons.adjust),text:'Ajustes')
         ]),
       ),

       body: new TabBarView(children: <Widget>[
         new Inicio(),
         new Registro(),         
         new Lugares(),
         new Ajustes()
         
       ],
       ),
     ),
     );

     return new MaterialApp(
       debugShowCheckedModeBanner: false,
       title:'Tabs con Flutter',
       theme: new ThemeData(
         primarySwatch:Colors.blue
       ),
       home: TabControl,
     );
     

  }
}
   