import 'package:flutter/material.dart';

class Ajustes extends StatelessWidget {
  var listas = [
    {"nombre": 'Bluetooth','icon':Icons.settings_bluetooth},
    {"nombre": 'Brillo','icon':Icons.settings_brightness},
    {"nombre": 'Enviar','icon':Icons.send},
    {"nombre": 'Seguridad','icon':Icons.security},
    {"nombre": 'Buscar','icon':Icons.search},
    {"nombre": 'Almacenamiento','icon':Icons.sd_card},
    {"nombre": 'Guardar','icon':Icons.save},
    {"nombre": 'Imprimir','icon':Icons.print},
    {"nombre": 'Foto','icon':Icons.photo},

    {"nombre": 'Correo','icon':Icons.email},
    {"nombre": 'Desarrolldor','icon':Icons.developer_mode},
    {"nombre": 'Compartir','icon':Icons.share},
    {"nombre": 'Sim-Card','icon':Icons.sim_card},
    {"nombre": 'Mensajes','icon':Icons.sms},
    {"nombre": 'Sincronizar','icon':Icons.sync},
    {"nombre": 'Vibracion','icon':Icons.vibration},
    {"nombre": 'Wi-Fi','icon':Icons.wifi},
    //{"nombre": 'Ecualizador','icon':Icons.equalizer},
  ];

  TextStyle styleText= TextStyle(color: Colors.red,fontSize: 25.0);

  Widget build (BuildContext context){
    return Scaffold(appBar: AppBar(title: Text('Ajustes'),
    ),
    body: ListView(children: _creatItemsCorta1()
    ));
  }

  List<Widget> _creatItemsCorta1(){
    return listas.map((item){
      return Column(
        children: <Widget>[
          ListTile(
            leading: Icon(item['icon'],color: Colors.blue),
            title: Text(item["nombre"].toString()),
            trailing: Icon(Icons.chevron_right,color: Colors.blue),
          ),
          
          Divider(),
          


        ],
      );
    }).toList();
  }
}