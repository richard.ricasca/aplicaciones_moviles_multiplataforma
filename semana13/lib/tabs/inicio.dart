import 'package:flutter/material.dart';



class Inicio extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new Stack(
        children:<Widget>[
          new Container(
            margin: const EdgeInsets.all(10.0),
            width: 350.0,
            height: 350.0,
            decoration: new BoxDecoration(
              image: new DecorationImage(image: AssetImage('assets/sf1.png'),
              fit: BoxFit.cover
              ),
            ),
          ),
          new Container(
            margin: const EdgeInsets.only(top:295.0 ,left:35.0),
            child: new RaisedButton(
              padding:  const EdgeInsets.only(top:0,left:0,right:0,bottom:10),
              textColor:Colors.white,
              color:Colors.red,
              onPressed: (){
                Scaffold
                .of(context)
                .showSnackBar(SnackBar(content: Text('Jugador: Ryu          Tecnica: Hadouken'),));
                
              },
              child: new Text('Detalles:'),
            ),
          ),

          new Container(
            child: RaisedButton(
              textColor: Colors.white,
              color: Colors.blue,
              onPressed: (){
                Scaffold
                .of(context)
                .showSnackBar(SnackBar(content:Text('Richard'),));
                
              },
              child: new Text('Saludo:'),
            ),
          ),


        ],
      )
      
    );
  }
}