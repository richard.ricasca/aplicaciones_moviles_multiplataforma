import 'package:flutter/material.dart';

class Email extends StatelessWidget {
  final options = ['Uno', 'Dos', 'Tres','Cuatro','Cinco'];  

  @override 
  Widget build (BuildContext context) {
    return Scaffold(appBar: AppBar(
      title: Text('Estas en Email'),
    ),
    body:Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children:[
         Image.asset('assets/email.PNG'),
         Icon(
           Icons.beach_access,
           color: Colors.blue,
           size: 36.0,
         ),
        ],
      ),
    ),
    );
    
  }
}
