import 'package:flutter/material.dart';

class Equalizador extends StatelessWidget {
  final options = ['Uno', 'Dos', 'Tres','Cuatro','Cinco'];  

  @override 
  Widget build (BuildContext context) {
    return Scaffold(appBar: AppBar(
      title: Text('Estas en Equalizador'),
    ),
    body:Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children:[
         Image.asset('assets/ecu.PNG'),
         Icon(
           Icons.beach_access,
           color: Colors.blue,
           size: 36.0,
         ),
        ],
      ),
    ),
    );
    
  }
}
