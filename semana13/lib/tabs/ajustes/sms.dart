import 'package:flutter/material.dart';

class Sms extends StatelessWidget {
  final options = ['Uno', 'Dos', 'Tres','Cuatro','Cinco'];  

  @override 
  Widget build (BuildContext context) {
    return Scaffold(appBar: AppBar(
      title: Text('Estas en Mensajes'),
    ),
    body:Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children:[
         Image.asset('assets/sms.PNG'),
         Icon(
           Icons.beach_access,
           color: Colors.blue,
           size: 36.0,
         ),
        ],
      ),
    ),
    );
    
  }
}
