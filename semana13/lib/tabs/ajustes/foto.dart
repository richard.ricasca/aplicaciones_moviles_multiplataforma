import 'package:flutter/material.dart';

class Foto extends StatelessWidget {
  final options = ['Uno', 'Dos', 'Tres','Cuatro','Cinco'];  

  @override 
  Widget build (BuildContext context) {
    return Scaffold(appBar: AppBar(
      title: Text('Estas en Foto'),
    ),
    body:Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children:[
         Image.asset('assets/foto.PNG'),
         Icon(
           Icons.beach_access,
           color: Colors.blue,
           size: 36.0,
         ),
        ],
      ),
    ),
    );
    
  }
}
