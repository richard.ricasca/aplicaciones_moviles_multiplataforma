import 'package:flutter/material.dart';

final _icons = <String, IconData>{
  
  'bt':Icons.settings_bluetooth,
  'send':Icons.send,
  'seguridad':Icons.security,
  'buscar':Icons.search,
  'sd':Icons.sd_card,
  'save':Icons.save,
  'imprimir':Icons.print,
  'foto':Icons.photo,
  'email':Icons.email,
  'developer':Icons.developer_mode,
  'compartir':Icons.share,
  'sim':Icons.sim_card,
  'sms':Icons.sms,
  'sinc':Icons.sync,
  'vibra':Icons.vibration,
  'wifi':Icons.wifi,
  'ecu':Icons.equalizer,
  'brillo':Icons.settings_brightness,
  
};

Icon getIcon(String nombreIcono){
  return Icon(_icons[nombreIcono]);
}