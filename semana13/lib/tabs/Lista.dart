
import 'package:flutter/material.dart';
import 'package:semana13/tabs/menu_provider.dart';



import 'icono_util.dart';

class Lista extends StatelessWidget{
  @override 

  Widget build (BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('NAVEGACION      Laboratorio 13'),
      ),
      body: _lista(),
    );
  }


Widget _lista(){
  return FutureBuilder(
    future:  menuProvider.cargarData(),
    initialData:  [],
    builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
      return ListView(children: _listaItems(snapshot.data, context));
    },
  );
 }

  List<Widget> _listaItems(List<dynamic> data, context){
    final List <Widget> opciones = [];

    data.forEach((opt) {
      final widgetTemp = ListTile(
        title: Text(opt['texto']),
        leading: getIcon(opt['icon']),
        trailing:Icon(Icons.arrow_forward_ios),
        onTap: () {
          print(opt['ruta']);
          Navigator.pushNamed(context, opt['ruta']);
        },

      );

      opciones..add(widgetTemp)..add(Divider());

    });
    return opciones;
  }

}




/*

import 'package:flutter/material.dart';

class Ajustes extends StatelessWidget {
  var listas = [
    {"nombre": 'Bluetooth','icon':Icons.settings_bluetooth},
    {"nombre": 'Brillo','icon':Icons.settings_brightness},
    {"nombre": 'Enviar','icon':Icons.send},
    {"nombre": 'Seguridad','icon':Icons.security},
    {"nombre": 'Buscar','icon':Icons.search},
    {"nombre": 'Almacenamiento','icon':Icons.sd_card},
    {"nombre": 'Guardar','icon':Icons.save},
    {"nombre": 'Imprimir','icon':Icons.print},
    {"nombre": 'Foto','icon':Icons.photo},

    {"nombre": 'Correo','icon':Icons.email},
    {"nombre": 'Desarrolldor','icon':Icons.developer_mode},
    {"nombre": 'Compartir','icon':Icons.share},
    {"nombre": 'Sim-Card','icon':Icons.sim_card},
    {"nombre": 'Mensajes','icon':Icons.sms},
    {"nombre": 'Sincronizar','icon':Icons.sync},
    {"nombre": 'Vibracion','icon':Icons.vibration},
    {"nombre": 'Wi-Fi','icon':Icons.wifi},
    {"nombre": 'Ecualizador','icon':Icons.equalizer},
  ];

  TextStyle styleText= TextStyle(color: Colors.red,fontSize: 25.0);

  Widget build (BuildContext context){
    return Scaffold(appBar: AppBar(title: Text('Ajustes'),
    ),
    body: ListView(children: _creatItemsCorta1()
    ));
  }

  List<Widget> _creatItemsCorta1(){
    return listas.map((item){
      return Column(
        children: <Widget>[
          ListTile(
            leading: Icon(item['icon'],color: Colors.blue),
            title: Text(item["nombre"].toString()),
            trailing: Icon(Icons.chevron_right,color: Colors.blue),
          ),
          Divider()
        ],
      );
    }).toList();
  }
}

*/